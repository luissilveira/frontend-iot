import { logging } from 'protractor';

export class Maquina{

    id: number;
    litrosAguaUtilizado: number;
    produtosProduzidos:number;
    quantidadeAcucar:number;
    status:boolean;

}