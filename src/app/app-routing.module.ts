import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaquinaComponent } from './admin/maquina/maquina.component';


const routes: Routes = [
  {path : 'maquina/:id', component : MaquinaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
