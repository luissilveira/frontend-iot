import { AfterViewInit, Component, OnInit } from '@angular/core';
import {Maquina} from '../../model/maquina';
import { Observable, throwError } from 'rxjs';
import {MaquinaService} from '../maquina/maquina.service';
import {ActivatedRoute} from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-maquina',
  templateUrl: './maquina.component.html',
  styleUrls: ['./maquina.component.css']
})

export class MaquinaComponent implements OnInit,AfterViewInit {

  maquina = {} as Maquina;
  userId = 0;
  var

  constructor(private maquinaService: MaquinaService, private route: ActivatedRoute ) {
    this.route.params.subscribe(params => this.userId = params['id']);
  }


  ngOnInit(): void {
    
      this.maquinaService.getCarById(this.userId).subscribe((res)=>{
        this.maquina = res;
      })

      $(".carregando").hide();
      
    
    
  }
  
  ngAfterViewInit(): void{

    

    setInterval(() => {
      
      this.maquinaService.getCarById(this.userId).subscribe((res)=>{
        this.maquina = res;
      })
      $(".carregando").hide();
      $(".btns-comando").show();

   }, 3000);
    
  }
  

  getCar(id: number){

    return this.maquinaService.getCarById(id);
    
  }
  changeStatus() {
    
    $(".btns-comando").hide();
    $(".carregando").show();

    this.maquinaService.alterStatus(this.userId).subscribe((res)=>{


    })
  }

 



}
