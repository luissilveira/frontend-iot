import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Maquina } from '../../model/maquina';

@Injectable({
  providedIn: 'root'
})

export class MaquinaService {

  url = 'http://localhost:8080/maquina'; // api rest fake

  constructor(private httpClient: HttpClient) { }

   // Headers
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  

  // Obtem um carro pelo id
  getCarById(id: number): Observable<Maquina> {
    return this.httpClient.get<Maquina>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  alterStatus(id: number): Observable<boolean> {
    return this.httpClient.patch<boolean>(this.url + '/status/' + id,null)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
      console.log(errorMessage)
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
      console.log(errorMessage)
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };


}
